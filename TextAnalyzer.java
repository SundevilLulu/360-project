import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JButton;
import javax.swing.JRadioButton;

public class TextAnalyzer {

	private JFrame frmTextFileAnalyzer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TextAnalyzer window = new TextAnalyzer();
					window.frmTextFileAnalyzer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TextAnalyzer() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTextFileAnalyzer = new JFrame();
		frmTextFileAnalyzer.setTitle("Text File Analyzer");
		frmTextFileAnalyzer.setBounds(100, 100, 528, 308);
		frmTextFileAnalyzer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTextFileAnalyzer.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("44px"),
				ColumnSpec.decode("218px"),
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				FormSpecs.DEFAULT_COLSPEC,},
			new RowSpec[] {
				RowSpec.decode("30px"),
				RowSpec.decode("16px"),
				RowSpec.decode("22px"),
				RowSpec.decode("16px"),
				RowSpec.decode("26px"),
				RowSpec.decode("16px"),
				RowSpec.decode("25px"),
				RowSpec.decode("16px"),
				RowSpec.decode("27px"),
				RowSpec.decode("16px"),
				RowSpec.decode("25px"),
				RowSpec.decode("16px"),}));
		
		JLabel lblNewLabel = new JLabel("File name:");
		frmTextFileAnalyzer.getContentPane().add(lblNewLabel, "2, 2, left, top");
		
		JLabel lblNumberOfWords = new JLabel("Number of words processed:");
		frmTextFileAnalyzer.getContentPane().add(lblNumberOfWords, "2, 4, fill, top");
		
		JButton btnInputFile = new JButton("Input File");
		frmTextFileAnalyzer.getContentPane().add(btnInputFile, "10, 5");
		
		JLabel lblNumberOfLines = new JLabel("Number of lines:");
		frmTextFileAnalyzer.getContentPane().add(lblNumberOfLines, "2, 6, fill, top");
		
		JRadioButton rdbtnLeftJustification = new JRadioButton("Left Justification");
		frmTextFileAnalyzer.getContentPane().add(rdbtnLeftJustification, "10, 7");
		
		JLabel lblOf = new JLabel("Number of blank lines removed:");
		frmTextFileAnalyzer.getContentPane().add(lblOf, "2, 8, fill, top");
		
		JRadioButton rdbtnRightJustification = new JRadioButton("Right Justification");
		frmTextFileAnalyzer.getContentPane().add(rdbtnRightJustification, "10, 9");
		
		JLabel lblAverageWordsPer = new JLabel("Average words per line:");
		frmTextFileAnalyzer.getContentPane().add(lblAverageWordsPer, "2, 10, fill, top");
		
		JButton btnOutputFile = new JButton("Output File");
		frmTextFileAnalyzer.getContentPane().add(btnOutputFile, "10, 11");
		
		JLabel lblAverageLineLength = new JLabel("Average line length:");
		frmTextFileAnalyzer.getContentPane().add(lblAverageLineLength, "2, 12, left, top");
	}
}
